/*
 * Neon
 * Copyright (C) 2018   REAL-TIME CONSULTING
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */
/** @file
 *  @author      Nenad Radulovic
 *  @brief       Tests for nlqueue
 *
 *  @addtogroup  module
 *  @{
 */
/** @defgroup    mod_test_nlqueue Tests for nlqueue
 *  @brief       Tests for nlqueue.
 *  @{
 */
/*---------------------------------------------------------------------------*/

#ifndef TEST_LQUEUE_H_
#define TEST_LQUEUE_H_

#ifdef __cplusplus
extern "C" {
#endif

void test_nqueue_lqueue(void);

#ifdef __cplusplus
}
#endif

/** @} */
/** @} */
/*---------------------------------------------------------------------------*/
#endif  /* TEST_LQUEUE_H_ */
